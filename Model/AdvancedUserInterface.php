<?php

namespace FOS\UserBundle\Model;

use Symfony\Component\Security\Core\User\UserInterface;

interface AdvancedUserInterface extends UserInterface
{
    public function isAccountNonExpired();
    public function isAccountNonLocked();
    public function isCredentialsNonExpired();
    public function isEnabled();
}